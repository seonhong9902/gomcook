import React from "react";
import {
  StyleSheet,
  View,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { theme } from "../../assets/styles/palette";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { useTranslation } from "react-i18next";

const SetMyAddress = ({ navigation }) => {
  const { t, i18n } = useTranslation();
  const currList = [
    {
      id: 1,
      address: "서울시 영등포구 선유로 9길 10",
    },
    {
      id: 2,
      address: "서울시 용산구 후암동",
    },
    {
      id: 3,
      address: "서울시 관악구 은천로",
    },
  ];
  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.firstView}>
        <View style={styles.firstInput}>
          <TouchableOpacity
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              paddingLeft: 10,
              paddingRight: 10,
            }}
            onPress={() => navigation.push("SearchGeo")}
          >
            <Text style={{ marginLeft: 5 }}>{t("searchPlaces")}</Text>
            <IconMaterial
              name="magnify"
              size={30}
              style={{ marginTop: 5 }}
              onPress={() => navigation.push("SearchGeo")}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.secondView}>
        <View style={styles.secondInput}>
          <IconMaterial name="map-marker-outline" size={30} />
          <View style={styles.secondINView}>
            <Text>{`${t("setCurrLoc")}`}</Text>
            <IconMaterial
              name="chevron-right"
              size={25}
              style={{ marginTop: 3 }}
              onPress={() =>
                navigation.push("SubPage", {
                  screen: "MyMap",
                })
              }
            />
          </View>
        </View>
      </View>
      <View style={styles.thirdView}>
        <Text style={styles.headerText}>{`${t("searchHistory")}`}</Text>
        <View>
          {currList.map((list) => {
            return (
              <View key={list.id} style={styles.cardContainer}>
                <Text>{list.address}</Text>
                <IconMaterial name="close" size={20} />
              </View>
            );
          })}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default SetMyAddress;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "teal",
  },
  firstView: {
    height: 60,
    justifyContent: "center",
  },
  firstInput: {
    flexDirection: "row",
    backgroundColor: `${theme.backColor}`,
  },
  secondView: {
    height: 70,
    justifyContent: "center",
    backgroundColor: `white`,
  },
  secondInput: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 8,
  },
  secondINView: {
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  thirdView: {
    marginTop: 20,
    backgroundColor: `white`,
  },
  headerText: {
    fontSize: 17,
    fontWeight: "600",
    margin: 15,
  },
  cardContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
    padding: 15,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: `${theme.backColor}`,
  },
});
