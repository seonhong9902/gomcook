/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from "react";
import Geolocation from "react-native-geolocation-service";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
  Platform,
  PermissionsAndroid,
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import { TextInput } from "react-native-gesture-handler";
import { theme } from "../../assets/styles/palette";
import BottomButton from "../../assets/styles/common/BottomButton";
import { useTranslation } from "react-i18next";

async function requestPermission() {
  try {
    // ios 구글 맵 정보 수집 권한 요청
    if (Platform.OS === "ios") {
      return await Geolocation.requestAuthorization("always");
    }
    // 안드로이드 구글 맵 정보 수집 권한 요청
    if (Platform.OS === "android") {
      return await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
    }
  } catch (e) {
    console.log(e);
  }
}

function MyMap({ navigation }) {
  const { t, i18n } = useTranslation();
  const [location, setLocation] = useState();

  useEffect(() => {
    requestPermission().then((result) => {
      console.log({ result });
      if (result === "granted") {
        Geolocation.getCurrentPosition(
          (pos) => {
            setLocation(pos.coords);
          },
          (err) => {
            console.log(err);
          },
          {
            enableHighAccuracy: true,
            timeout: 3600,
            maximumAge: 3600,
          }
        );
      }
    });
  }, []);

  if (!location) {
    return (
      <View style={{ flex: 1 }}>
        <Text style={{ flex: 1 }}>Splash Screen</Text>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        }}
        style={{
          flex: 2,
          width: "100%",
          height: "100%",
        }}
      ></MapView>
      <View style={styles.bottom}>
        <Text style={styles.bottomText}>서울 영등포구 선유로 8길 23 dd</Text>
        <TextInput
          style={styles.bottomInput}
          placeholder={`${t("InputDetailLoc")}`}
        />
        <BottomButton title_text={`${t("setLoc")}`} />
      </View>
    </View>
  );
}

export default MyMap;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  marker: {
    borderWidth: 10,
    borderRadius: 50,
    borderColor: "rgb(255, 102, 0)",
  },
  bottom: {
    flex: 1,
    backgroundColor: `${theme.backColor}`,
    justifyContent: "center",
  },
  bottomText: {
    fontSize: 20,
    fontWeight: "600",
    color: "black",
    paddingLeft: 8,
  },
  bottomInput: {
    backgroundColor: "white",
    borderTopColor: `lightgray`,
    borderBottomColor: "lightgray",
    marginTop: 15,
    marginBottom: 10,
    paddingLeft: 10,
  },
});
