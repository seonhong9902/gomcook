import React from "react";
import { View, Text, StyleSheet, KeyboardAvoidingView } from "react-native";
import Geocoder from "react-native-geocoding";
import { TextInput } from "react-native-gesture-handler";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { theme } from "../../assets/styles/palette";
import { useTranslation } from "react-i18next";

const SearchGeo = () => {
  const { t, i18n } = useTranslation();
  const addressSelect = (roadAddr) => {
    Geocoder.init("AIzaSyBuIO4SzW-IV5jtHS7pvQfkbliR1Hn1eEM");
    Geocoder.from(roadAddr)
      .then((json) => {
        var location = json.results[0].geometry.location;
        // console.log(location.lat);
        // console.log(JSON.results);
        console.log(location);
        // setInitLat(location.lat);
        // setInitLng(location.lng);
      })
      .catch((error) => console.warn(error));
    // setSelectAddress(roadAddr);
    // setSearch(false);
    // onClose();
  };

  const _getCurrentPosition = async () => {
    Geocoder.init("AIzaSyBuIO4SzW-IV5jtHS7pvQfkbliR1Hn1eEM"); // initialized with a valid API key
    const status = await locationPermission(); // write the function
    if (status === "granted") {
      const location = await getCurrentLocation(); // write the function
      console.log("Location Permission granted!");
      console.log({ currentLoc: location });

      Geocoder.from({
        latitude: location.latitude,
        longitude: location.longitude,
      })
        .then((addressJson) => {
          const _location = addressJson.results[0].formatted_address;
          console.log("location_", _location);
        })
        .catch((error) => console.warn(error));

      //or for non-geometry positions (plain addresses)
      //**IMPORTANT** make sure the address is valid to avoid errors
      //one way to do this is to first google a valid address
      Geocoder.from("visakhapatnam")
        .then((addressJson) => {
          const _location = addressJson.results[0].geometry.location;
          console.log("location_", _location);
        })
        .catch((error) => console.warn(error));
    } else {
      console.log("Permission not grantedd!");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchView}>
        <TextInput
          placeholder={`${t("searchPlaces")}`}
          style={{ backgroundColor: "white" }}
        />
        <IconMaterial name="magnify" size={30} onPress={addressSelect} />
      </View>
      <Text>helloooo</Text>
    </View>
  );
};
export default SearchGeo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  searchView: {
    height: 70,
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: `${theme.backColor}`,
    borderStyle: "solid",
  },
});
