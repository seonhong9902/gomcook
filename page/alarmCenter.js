import React from "react";
import { useState } from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import { theme } from "../assets/styles/palette";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { useTranslation } from "react-i18next";

const AlarmCenter = ({ navigation }) => {
  const [unCheckedAlarm, setUnCheckedAlarm] = useState(false);
  const { t, i18n } = useTranslation();

  const alarmList = [
    {
      alarm_id: 1,
      alarm_title: "조리가 완료되었습니다.",
      alarm_content: "맛있게 드시고 리뷰를 남겨 주세요.",
      alarm_date: "5월 20일 11:50",
    },
    {
      alarm_id: 2,
      alarm_title: "조리가 완료되었습니다.",
      alarm_content:
        "주문하신 들깨 투움바 파스타가 도착하였습니다. 맛있게 드세요!",
      alarm_date: "5월 26일 16:20",
    },
    {
      alarm_id: 3,
      alarm_title: "여름 맞이 냉소바 10% 할인!",
      alarm_content:
        "여름을 맞아 7월 20일부터 21일 단 이틀 동안 소바 할인 이벤트를 합니다! 많관부 ^^~~",
      alarm_date: "7월 15일 12:00",
    },
  ];

  return (
    <View style={styles.container}>
      {unCheckedAlarm ? (
        <View style={styles.uncheckedHeader}>
          <Text style={{ color: "white" }}>
            기기 설정 내 곰쿡 알림이 꺼져 있습니다. 알림을 받아 보시려면
            설정에서 알림 허용을 해주세요.
          </Text>
        </View>
      ) : (
        <>
          <View style={styles.checkedHeader}>
            <Text style={{ color: "white" }}>{t("pastNoti")}</Text>
          </View>
          <View style={styles.contentBox}>
            {alarmList.map((list) => {
              return (
                <View style={styles.content_container}>
                  <View key={list.alarm_id} style={styles.content}>
                    <Text style={styles.content_title}>{list.alarm_title}</Text>
                    <Text style={styles.content_content}>
                      {list.alarm_content}
                    </Text>
                    <Text style={styles.content_date}>{list.alarm_date}</Text>
                  </View>
                  <IconMaterial name="close" size={23} />
                </View>
              );
            })}
          </View>
        </>
      )}
    </View>
  );
};

export default AlarmCenter;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  uncheckedHeader: {
    height: 80,
    flexDirection: "row",
    backgroundColor: `${theme.subColor}`,
    alignItems: "center",
    padding: 20,
  },
  checkedHeader: {
    height: 50,
    flexDirection: "row",
    backgroundColor: `${theme.subColor}`,
    alignItems: "center",
    padding: 10,
    justifyContent: "center",
  },
  contentBox: {
    height: "100%",
  },
  content_container: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: `${theme.subColor}`,
    borderWidth: 0.5,
    borderStyle: "dotted",
  },
  content: {
    justifyContent: "center",
    padding: 20,
    width: "92%",
  },
  content_title: {
    fontSize: 15,
    fontWeight: "700",
  },
  content_content: {
    fontSize: 14,
    marginTop: 5,
  },
  content_date: {
    fontSize: 13,
    marginTop: 5,
  },
});
