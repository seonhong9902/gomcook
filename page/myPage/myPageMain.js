import React from "react";
import { View, Text, StyleSheet, Button, TouchableOpacity } from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { theme } from "../../assets/styles/palette";
import { useTranslation } from "react-i18next";

const MyPageMain = ({ navigation }) => {
  const { t, i18n } = useTranslation();
  const menuList = [
    {
      menu_id: 1,
      menu_name: "리뷰 작성",
      menu_togo: "LeaveReviews",
    },
    {
      menu_id: 2,
      menu_name: "이벤트",
      menu_togo: "",
    },
    {
      menu_id: 3,
      menu_name: "고객센터",
      menu_togo: "",
    },
    {
      menu_id: 4,
      menu_name: "설정",
      menu_togo: "",
    },
  ];
  return (
    <View style={styles.container}>
      <View style={styles.fixedView}>
        <View style={styles.userBox}>
          <View style={styles.userInfoBox}>
            <View style={styles.userProfileBox}>
              <View style={styles.userProfile}>
                <IconMaterial
                  name="account-outline"
                  size={55}
                  style={{ color: `${theme.textColor}` }}
                  onPress={() =>
                    navigation.push("SubPage", {
                      screen: "MyPageInfo",
                    })
                  }
                />
              </View>
              {/* <View style={styles.userEditProfile}>
                <IconMaterial
                  name="circle-edit-outline"
                  size={30}
                  style={{ color: `${theme.textColor}` }}
                  onPress={() =>
                    navigation.push("SubPage", {
                      screen: "MyPageInfo",
                    })
                  }
                />
              </View> */}
            </View>
            <View style={styles.userTextBox}>
              <Text style={styles.userText}>{t("goLogin")}</Text>
              <IconMaterial
                name="chevron-right"
                size={30}
                // onPress={() =>
                //   navigation.push("SubPage", {
                //     screen: "MyMap",
                //   })
                // }
              />
            </View>
          </View>
        </View>
        <View style={styles.orderText}>
          <Text style={{ fontWeight: "600", fontSize: 17 }}>주문 내역</Text>
        </View>
      </View>
      <View style={{ flex: 0.6 }}>
        {menuList.map((list) => {
          return (
            <TouchableOpacity
              key={list.menu_id}
              style={styles.menus}
              onPress={() =>
                navigation.push("SubPage", {
                  screen: `${list.menu_togo}`,
                })
              }
            >
              <Text style={styles.menuText}>{list.menu_name}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default MyPageMain;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  fixedView: {
    flex: 0.45,
    // backgroundColor: "tomato",
  },
  userBox: {
    flex: 0.65,
    justifyContent: "center",
    borderStyle: "solid",
    borderBottomColor: "lightgray",
    borderBottomWidth: 0.7,
    borderTopColor: "lightgray",
    borderTopWidth: 1,
  },
  userInfoBox: {
    flexDirection: "row",
    height: "70%",
    alignItems: "center",
    marginLeft: "10%",
  },
  userProfileBox: {
    width: "35%",
    position: "relative",
  },
  userProfile: {
    borderWidth: 1.5,
    borderStyle: `${theme.textColor}`,
    borderStyle: "solid",
    height: "70%",
    width: "80%",
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  userEditProfile: {
    position: "absolute",
    right: 10,
    bottom: -18,
  },
  userTextBox: {
    flexDirection: "row",
    // marginLeft: "2%",
    marginTop: "-5%",
  },
  userText: {
    fontSize: 16,
    fontWeight: "600",
    marginTop: 3,
  },
  orderText: {
    flex: 0.2,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "lightgray",
    borderBottomWidth: 0.7,
    borderStyle: "solid",
  },
  menus: {
    height: 30,
    borderBottomColor: "lightgray",
    borderBottomWidth: 0.7,
    borderStyle: "solid",
    marginBottom: 25,
    marginLeft: 10,
    marginRight: 10,
  },
  menuText: {
    fontSize: 17,
    fontWeight: "600",
  },
});
