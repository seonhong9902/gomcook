import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { theme } from "../../assets/styles/palette";
import { TextInput } from "react-native-gesture-handler";
import { RadioButton } from "react-native-paper";
import { useTranslation } from "react-i18next";

const EditUserInfo = ({ navigation }) => {
  const { t, i18n } = useTranslation();
  const [checked, setChecked] = useState("캐릭터 1");
  const charList = [
    {
      char_id: 1,
      char_name: "캐릭터 1",
      char_img: "../../assets/images/default_logo.png",
    },
    {
      char_id: 2,
      char_name: "캐릭터 2",
      char_img: "../../assets/images/default_logo.png",
    },
    {
      char_id: 3,
      char_name: "캐릭터 3",
      char_img: "../../assets/images/default_logo.png",
    },
    {
      char_id: 4,
      char_name: "캐릭터 4",
      char_img: "../../assets/images/default_logo.png",
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.headerBox}>
        <Text style={styles.headerText}>GOMCOOK</Text>
      </View>
      <ScrollView>
        <View style={styles.infoMenu}>
          <IconMaterial
            name="brightness-1"
            size={6}
            style={{ color: `${theme.textColor}`, marginRight: 10 }}
          />
          <Text style={styles.infoText}>{t("id")} : podosee@naver.com</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial
            name="brightness-1"
            size={6}
            style={{ color: `${theme.textColor}`, marginRight: 10 }}
          />
          <Text style={styles.infoText}>{t("password")} : ********</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial
            name="brightness-1"
            size={6}
            style={{ color: `${theme.textColor}`, marginRight: 10 }}
          />
          <Text style={styles.infoText}>{t("userGender")}: 여</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial
            name="brightness-1"
            size={6}
            style={{ color: `${theme.textColor}`, marginRight: 10 }}
          />
          <Text style={styles.infoText}>{t("userBirthDate")} : YYYY.MM.DD</Text>
        </View>

        <View style={styles.columnView}>
          <View style={styles.infoMenu2}>
            <IconMaterial
              name="brightness-1"
              size={6}
              style={{ color: `${theme.textColor}`, marginRight: 10 }}
            />
            <Text style={styles.infoText}>{t("typeInName")}</Text>
          </View>
          <TextInput placeholder={t("typeInName")} style={styles.inputBox} />
        </View>

        <View style={styles.columnView}>
          <View style={styles.infoMenu2}>
            <IconMaterial
              name="brightness-1"
              size={6}
              style={{ color: `${theme.textColor}`, marginRight: 10 }}
            />
            <Text style={styles.infoText}>{t("nickname")}</Text>
          </View>
          <TextInput placeholder="제갈길동" style={styles.inputBox} />
        </View>

        <View style={styles.columnView}>
          <View style={styles.infoMenu2}>
            <IconMaterial
              name="brightness-1"
              size={6}
              style={{ color: `${theme.textColor}`, marginRight: 10 }}
            />
            <Text style={styles.infoText}>{t("chooseChar")}</Text>
          </View>
          <View style={styles.characterBox}>
            {charList.map((list) => {
              return (
                <View key={list.char_id} style={styles.characterImgBox}>
                  <RadioButton
                    value={list.char_name}
                    status={
                      checked === `${list.char_name}` ? "checked" : "nuchecked"
                    }
                    onPress={() => setChecked(list.char_name)}
                  />
                  <View style={styles.characterImg}></View>
                  <Text style={styles.characterName}>{list.char_name}</Text>
                </View>
              );
            })}
          </View>
        </View>

        <View style={styles.columnView}>
          <View style={styles.infoMenu2}>
            <IconMaterial
              name="brightness-1"
              size={6}
              style={{ color: `${theme.textColor}`, marginRight: 10 }}
            />
            <Text style={styles.infoText}>{t("userNumber")}</Text>
          </View>

          <View style={styles.numberInput}>
            <Text>대한민국</Text>
            <IconMaterial name="plus" size={27} style={{ color: ` ` }} />
            <Text style={{ fontSize: 17, color: `gray` }}>82</Text>
            <IconMaterial
              name="menu-down"
              size={30}
              style={styles.selectDown}
            />
          </View>

          <View style={styles.numberInput2}>
            <TextInput
              placeholder={t("typeInMobile")}
              style={styles.numberInput3}
            />
            <TouchableOpacity style={styles.numberButton}>
              <Text>{t("getAuthNum")}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.numberInput2}>
            <TextInput
              placeholder={t("typeInAuthNum")}
              style={styles.numberInput3}
            />
            <TouchableOpacity style={styles.numberButton}>
              <Text>{t("checkAuthNum")}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.footerBox}>
          <TouchableOpacity
            style={styles.footerEditBtn}
            onPress={() =>
              navigation.push("SubPage", {
                screen: "MyPageInfo",
              })
            }
          >
            <Text style={{ color: `${theme.textColor}` }}>
              {t("editMyInfo")}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default EditUserInfo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: "5%",
    paddingRight: "5%",
    backgroundColor: "white",
  },
  headerBox: {
    height: 100,
    alignItems: "center",
  },
  headerText: {
    fontSize: 23,
    fontWeight: "700",
    color: `${theme.textColor}`,
    marginTop: "5%",
    marginBottom: "5%",
  },
  infoMenu: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  infoMenu2: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 7,
  },
  infoText: {
    fontSize: 17,
    fontWeight: "700",
  },
  inputBox: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "lightgray",
    borderRadius: 5,
    paddingLeft: 10,
  },
  columnView: {
    justifyContent: "center",
    marginBottom: 20,
  },
  numberInput: {
    flexDirection: "row",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "lightgray",
    paddingLeft: 10,
    paddingRight: 5,
    borderRadius: 5,
    marginBottom: 10,
    height: 50,
    alignItems: "center",
    position: "relative",
  },
  numberInput2: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  selectDown: {
    position: "absolute",
    right: 5,
    color: `${theme.textColor}`,
  },
  numberInput3: {
    borderStyle: "solid",
    borderColor: "lightgray",
    borderWidth: 1,
    justifyContent: "center",
    height: 50,
    width: "67%",
    borderRadius: 5,
    paddingLeft: 10,
  },
  numberButton: {
    borderStyle: "solid",
    borderColor: "lightgray",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "30%",
    height: 50,
    borderRadius: 5,
  },

  characterBox: {
    flexDirection: "row",
    width: "100%",
    height: 150,
  },
  characterImgBox: {
    width: "25%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  characterImg: {
    borderRadius: 50,
    borderStyle: "solid",
    borderColor: `${theme.textColor}`,
    borderWidth: 1,
    width: "85%",
    height: "50%",
    marginTop: 7,
  },
  characterName: {
    color: `${theme.textColor}`,
    fontSize: 16,
    fontWeight: "500",
    marginTop: 7,
  },

  footerBox: {
    flexDirection: "row",
    width: "100%",
    height: "5%",
    justifyContent: "center",
    marginTop: 60,
    marginBottom: 15,
  },
  footerEditBtn: {
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 0.7,
    borderColor: `${theme.textColor}`,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 50,
  },
});
