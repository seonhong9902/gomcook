import React from "react";
import { View, Text, StyleSheet, Button, TouchableOpacity } from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { theme } from "../../assets/styles/palette";
import { useTranslation } from "react-i18next";

const MyPageInfo = ({ navigation }) => {
  const { t, i18n } = useTranslation();
  return (
    <View style={styles.container}>
      <View style={styles.profileImgBox}>
        <Text style={styles.headerText}>GOMCOOK</Text>
        <View style={styles.profileContainer}>
          <View style={styles.profileBox}>
            <View style={styles.profileFrame}>
              <IconMaterial
                name="account-outline"
                size={65}
                style={{ color: `${theme.textColor}` }}
              />
            </View>
          </View>
          <View style={styles.nicknameBox}>
            <Text style={styles.nicknameText}>닉네임</Text>
          </View>
        </View>
      </View>

      <View style={styles.infoBox}>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("id")} : podosee@naver.com</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("password")} : *********</Text>
          <TouchableOpacity
            style={styles.editPWBtn}
            onPress={() =>
              navigation.push("SubPage", {
                screen: "EditMyPassword",
              })
            }
          >
            <Text>변경하기</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("userName")} : 제갈길동</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("userBirthDate")} : YYYY.MM.DD</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("userGender")} : 여</Text>
        </View>
        <View style={styles.infoMenu}>
          <IconMaterial name="brightness-1" size={8} style={{ padding: 13 }} />
          <Text style={styles.infoText}>{t("userNumber")} : 010-1234-1234</Text>
        </View>
      </View>

      <View style={styles.footerBox}>
        <TouchableOpacity
          style={styles.footerEditBtn}
          onPress={() =>
            navigation.push("SubPage", {
              screen: "EditUserInfo",
            })
          }
        >
          <Text style={{ color: `${theme.textColor}` }}>{t("editMyInfo")}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MyPageInfo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  profileImgBox: {
    flex: 0.35,
    // justifyContent: "center",
    alignItems: "center",
  },
  headerText: {
    fontSize: 23,
    fontWeight: "700",
    color: `${theme.textColor}`,
    marginTop: "5%",
    marginBottom: "5%",
  },
  profileContainer: {
    // justifyContent: "center",
    height: "60%",
    width: "50%",
  },
  profileBox: {
    height: "70%",
    width: "100%",
    // justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  profileFrame: {
    justifyContent: "center",
    alignItems: "center",
    borderColor: `${theme.textColor}`,
    borderStyle: "solid",
    borderWidth: 1.2,
    borderRadius: 200,
    width: "55%",
    height: "95%",
  },
  nicknameBox: {
    height: 25,
    alignItems: "center",
    marginTop: "10%",
  },
  nicknameText: {
    fontSize: 18,
    color: `${theme.textColor}`,
    fontWeight: "500",
  },
  infoBox: {
    flex: 0.55,
    paddingLeft: "5%",
  },
  infoMenu: {
    flexDirection: "row",
    alignItems: "center",
    position: "relative",
  },
  infoText: {
    color: `${theme.textColor}`,
    fontSize: 17,
    fontWeight: "400",
    marginBottom: 12,
    marginTop: 5,
  },
  editPWBtn: {
    position: "absolute",
    right: "5%",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: `lightgray`,
    borderRadius: 5,
    width: 85,
    height: 30,
    alignItems: "center",
    paddingTop: 3,
  },
  footerBox: {
    flex: 0.1,
    flexDirection: "row",
    width: "100%",
    height: "5%",
    justifyContent: "center",
  },
  footerEditBtn: {
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 0.7,
    borderColor: `${theme.textColor}`,
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    height: 50,
  },
});
