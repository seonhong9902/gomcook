import React from "react";
import { View, Text, StyleSheet, Button, TouchableOpacity } from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { theme } from "../../assets/styles/palette";
import { TextInput } from "react-native-gesture-handler";
import { useTranslation } from "react-i18next";

const EditMyPassword = ({ navigation }) => {
  const { t, i18n } = useTranslation();
  return (
    <View style={styles.container}>
      <View style={styles.headerBox}>
        <IconMaterial name="brightness-1" size={6} style={{ marginRight: 8 }} />
        <Text style={styles.headerText}>{t("resetMobile")}</Text>
      </View>

      <View style={styles.inputBox}>
        <TextInput placeholder={t("typeInName")} style={styles.textInput} />

        <View style={styles.textInput2}>
          <Text>대한민국</Text>
          <IconMaterial
            name="plus"
            size={27}
            style={{ color: `${theme.textColor}` }}
          />
          <Text style={{ fontSize: 17, color: `${theme.textColor}` }}>82</Text>
          <IconMaterial name="menu-down" size={30} style={styles.selectDown} />
        </View>

        <View style={styles.textInput3}>
          <TextInput
            placeholder={t("typeInMobile")}
            style={styles.numberInput}
          />
          <TouchableOpacity style={styles.numberButton}>
            <Text>{t("getAuthNum")}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.textInput3}>
          <TextInput
            placeholder={t("typeInAuthNum")}
            style={styles.numberInput}
          />
          <TouchableOpacity style={styles.numberButton}>
            <Text>{t("checkAuthNum")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default EditMyPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  headerBox: {
    height: 65,
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "5%",
  },
  headerText: {
    fontSize: 15,
    fontWeight: "500",
    color: `${theme.textColor}`,
  },
  inputBox: {
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  textInput: {
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "lightgray",
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 5,
    marginBottom: 10,
    height: 50,
  },
  textInput2: {
    flexDirection: "row",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "lightgray",
    paddingLeft: 10,
    paddingRight: 5,
    borderRadius: 5,
    marginBottom: 10,
    height: 50,
    alignItems: "center",
    position: "relative",
  },
  selectDown: {
    position: "absolute",
    right: 5,
    color: `${theme.textColor}`,
  },
  textInput3: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  numberInput: {
    borderStyle: "solid",
    borderColor: "lightgray",
    borderWidth: 1,
    justifyContent: "center",
    height: 50,
    width: "67%",
    borderRadius: 5,
    paddingLeft: 10,
  },
  numberButton: {
    borderStyle: "solid",
    borderColor: "lightgray",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "30%",
    height: 50,
    borderRadius: 5,
  },
});
