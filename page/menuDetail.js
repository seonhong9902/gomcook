/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect, useRef } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
//import { ImageHeaderScrollView, TriggeringView } from 'react-native-image-header-scroll-view';
import  ImageHeaderScrollView , { TriggeringView } from 'react-native-image-header-scroll-view';
import * as Animatable from 'react-native-animatable';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import BouncyCheckbox from 'react-native-bouncy-checkbox';

function MenuDetail({route, navigation}) {

    console.log(route.params);

  const [titleView, setTitleView] = useState(false);

  const [checkList, setCheckList] = useState([]);

  const check = (list) => {
        console.log(checkList);
        // checkList.indexOf(list) >= -1 ?
        // setCheckList(list) : 
  };

  const titleViewChange = () => {
    if(titleView == true){
      setTitleView(false);
      navigation.setOptions({
        title:'',
        //headerShown: false,
    headerLeft:()=>(
        <IconMaterial
        size={25}
        style={{paddingLeft:10}}
        name="keyboard-arrow-left"
        color={'white'}
        onPress={()=>navigation.goBack()}
    />
    ),
    headerRight:()=>(
        <View style={{
            flexDirection:'row',
            marginRight:10,
            width:70,
            justifyContent:'space-between'
        }}>
          
    <IconFeather
        size={25}
        name="home"
        color={'white'}
    />
    <View>
    
        <IconFeather
        size={25}
        name="shopping-bag"
        color={'white'}
    />
            </View>
        
             </View>
    ),
    headerTransparent: true,
        
     })
    }else{
      setTitleView(true);
      navigation.setOptions({
        title:'',
        //headerShown: false,
    headerLeft:()=>(
        <IconMaterial
        size={25}
        style={{paddingLeft:10}}
        name="keyboard-arrow-left"
        color={'black'}
        onPress={()=>navigation.goBack()}
    />
    ),
    headerRight:()=>(
        <View style={{
            flexDirection:'row',
            marginRight:10,
            width:70,
            justifyContent:'space-between'
        }}>
          
    <IconFeather
        size={25}
        name="home"
        color={'black'}
    />
    <View>
    
        <IconFeather
        size={25}
        name="shopping-bag"
        color={'black'}
    />
            </View>
        
             </View>
    ),
    headerTransparent: true,
        
     })
    }
  }

  return (
    <View style={{ flex: 1 }}>
   <ImageHeaderScrollView
    minHeight={50}
    maxHeight={Dimensions.get('window').height/3}
       minOverlayOpacity={0}
      maxOverlayOpacity={1}
      overlayColor={'white'}

      renderFixedForeground={() => (
        titleView ? 
        <Animatable.View style={{height:50,
          justifyContent:'center',
          width:'50%',
          alignSelf:'center'}}>
          <Text ellipsizeMode='tail' numberOfLines={1} 
          style={{color:'black', textAlign:'center', fontSize:20,
          }}>{route.params.title}</Text>
        </Animatable.View> : <></>
      )}
    
      renderHeader={() => (
      <Image
        style={{
          width:Dimensions.get('window').width,
          height:Dimensions.get('window').height/3
        }}
        resizeMode='cover'
        source={{uri:route.params.thumbnail}}/>)}
  >

    <TriggeringView
   // onHide={() => console.log("On Hide")}
    onDisplay={() =>  titleViewChange()}
    onBeginHidden={() => titleViewChange()}
   // onBeginDisplayed={() => console.log("OnBeginDisPlayed")}
    >
      <View style={{width:'90%', alignSelf:'center'}}>
        <View style={{width:'100%', 
        flexDirection:'column',
         justifyContent:'center',
          paddingBottom:20,
          paddingTop:20}}>
          <View style={{flexDirection:'column', 
            justifyContent:'center',
          paddingBottom:5}}>
          <Text style={{fontSize:22, 
          paddingBottom:10,
            fontWeight:'bold',
             color:'black'}}>{route.params.title}</Text>
          <Text style={{fontSize:15, 
          paddingBottom:10,
            fontWeight:'400',
             color:'rgb(118, 118, 118)'}}>{route.params.content}</Text>
    </View>

    <View style={{flexDirection:'row'}}>
        <TouchableOpacity
            style={{
                borderColor:'rgb(219, 219, 219)',
                borderWidth:1,
                borderRadius:5
            }}>
            <Text style={{fontSize:15,
            padding:5,
            fontWeight:'400',
             color:'black'}}>
                재료정보
             </Text>
            </TouchableOpacity>
            <View style={{width:10}}></View>
            <TouchableOpacity
            style={{
                borderColor:'rgb(219, 219, 219)',
                borderWidth:1,
                borderRadius:5
            }}>
            <Text style={{fontSize:15,
            padding:5,
            fontWeight:'400',
             color:'black'}}>
                알레르기 유발 정보
             </Text>
            </TouchableOpacity>
    </View>

        </View>

      <View style={{
        borderColor:'grey',
        borderBottomWidth:0.5, 
        borderTopWidth:0.5,
        paddingVertical:20,
      }}>
        <Text style={{color:'black', fontSize:16, paddingBottom:20, fontWeight:'400'}}>가격</Text>

        <BouncyCheckbox
            style={{marginBottom:10}}
              size={24}
              fillColor={'rgb(255, 155, 4)'}
              //#add482,   gray
              text="메뉴 1번"
              textStyle={{
                fontSize:16,
                textDecorationLine: 'none',
                color: 'black'
              }}
              innerIconStyle={{borderRadius: 0}}
              iconStyle={{borderRadius: 0}}
              onPress={() => check("메뉴 1번")}
            />

<BouncyCheckbox
            style={{marginBottom:10}}
              size={24}
              fillColor={'rgb(255, 155, 4)'}
              //#add482,   gray
              text="메뉴 2번"
              textStyle={{
                fontSize:16,
                textDecorationLine: 'none',
                color: 'black'
              }}
              innerIconStyle={{borderRadius: 0}}
              iconStyle={{borderRadius: 0}}
              onPress={() => check("메뉴 2번")}
            />

<BouncyCheckbox
            style={{marginBottom:10}}
              size={24}
              fillColor={'rgb(255, 155, 4)'}
              //#add482,   gray
              text="메뉴 3번"
              textStyle={{
                fontSize:16,
                textDecorationLine: 'none',
                color: 'black'
              }}
              innerIconStyle={{borderRadius: 0}}
              iconStyle={{borderRadius: 0}}
              onPress={() => check("메뉴 3번")}
            />
            
      </View>
      <Text style={{color:'grey', fontSize:16, paddingBottom:20, fontWeight:'bold'}}>{route.params.title}</Text>
      
      <View style={{width:'100%', height:60}}/>
      </View>
    </TriggeringView>

        </ImageHeaderScrollView>

        <View style={{position:'absolute',
         width:'90%',
          height:50, 
          backgroundColor:'rgba(255, 102, 0, 1)', 
          alignSelf:'center',
          borderRadius:5,
          alignItems:'center',
          justifyContent:'center',
          bottom:10}}>

 <Text style={{fontSize:18, color:'white', fontWeight:'400'}}>장바구니 가기 32,900원</Text>
            </View>
    </View>
    
  );
};



export default MenuDetail;

const styles = StyleSheet.create({
  header: {
    height: 150,
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    backgroundColor: "transparent",
    color: "#fff",
    fontSize: 24
  },
  content: {
    padding: 20,
    height:1000
  },
  title: {
    fontSize: 36,
    fontWeight: "bold",
    marginBottom: 20
  },
  paragraph: {
    fontSize: 18,
    marginBottom: 20
  }
});
