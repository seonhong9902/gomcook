/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
} from "react-native";
import Swiper from "react-native-swiper";

function ShopInfo({ navigation }) {
  const renderPagination = (index, total, context) => {
    return (
      <View
        style={{
          position: "absolute",
          backgroundColor: "rgba(0, 0, 0, 0.6)",
          right: 20,
          bottom: 10,
          borderRadius: 10,
        }}
      >
        <Text
          style={{
            color: "white",
            fontSize: 12,
            fontFamily: "KoPubWorldDotumBold",
            paddingHorizontal: 10,
          }}
        >
          1/1
        </Text>
      </View>
    );
  };

  return (
    <ScrollView
      style={{ width: "100%", alignSelf: "center", backgroundColor: "white" }}
    >
      <View
        style={{
          width: "100%",
          backgroundColor: "white",
          borderBottomColor: "rgb(241, 241, 241)",
          borderBottomWidth: 10,
          paddingHorizontal: "5%",
          paddingVertical: 20,
        }}
      >
        <Text
          style={{
            color: "black",
            fontSize: 18,
            fontWeight: "400",
            marginBottom: 10,
          }}
        >
          놀부김치찜 - 정부청사역점
        </Text>
        <View
          style={{ flexDirection: "column", width: "100%", paddingTop: 10 }}
        >
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-start",
            }}
          >
            <View style={{ flex: 0.5 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                주소
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                경기도 안양시 만안구 안양2동 뭐시기 뭐시기 뭐시기
                뭐시기ㄴㅁ아ㅓㅁ나움ㄴㅇㄴ무
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-start",
            }}
          >
            <View style={{ flex: 0.5 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                전화번호
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                010-2249-9902
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-start",
            }}
          >
            <View style={{ flex: 0.5 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                대표자명
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                박선홍
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-start",
            }}
          >
            <View style={{ flex: 0.5 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                사업자등록번호
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                124-45-23308
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View
        style={{
          width: "100%",
          backgroundColor: "white",
          borderBottomColor: "rgb(241, 241, 241)",
          borderBottomWidth: 10,
          paddingHorizontal: "5%",
          paddingVertical: 20,
        }}
      >
        <Text
          style={{
            color: "black",
            fontSize: 18,
            fontWeight: "400",
            marginBottom: 10,
          }}
        >
          영업시간
        </Text>
        <View style={{ flexDirection: "column", width: "100%" }}>
          {/*  요일별로 다를경우 해당부분 for문   */}
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "flex-start",
              paddingTop: 10,
            }}
          >
            <View style={{ flex: 0.5 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                월요일 - 일요일
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "rgb(118, 118, 118)",
                  fontSize: 15,
                  fontWeight: "400",
                  marginBottom: 10,
                }}
              >
                09:00~22:00 / 연중무휴
              </Text>
            </View>
          </View>

          {/*  요일별로 다를경우 해당부분 for문   */}
        </View>
      </View>

      <View
        style={{
          width: "100%",
          backgroundColor: "white",
          borderBottomColor: "rgb(241, 241, 241)",
          borderBottomWidth: 10,
          paddingHorizontal: "5%",
          paddingVertical: 20,
        }}
      >
        <Text
          style={{
            color: "black",
            fontSize: 18,
            fontWeight: "400",
            marginBottom: 10,
          }}
        >
          매장소개
        </Text>
        <View style={{ flexDirection: "column", width: "100%" }}>
          {/*  요일별로 다를경우 해당부분 for문   */}
          <View
            style={{
              flexDirection: "column",
              width: "100%",
              justifyContent: "flex-start",
              paddingTop: 10,
            }}
          >
            <Text
              style={{
                color: "rgb(118, 118, 118)",
                fontSize: 15,
                fontWeight: "400",
                marginBottom: 10,
              }}
            >
              품질 좋은 김치찜을 합리적인 가격으로 제공하는 김치찜 전문점입니다.
              주문 즉시 조리하며 뭐시기 뭐시기
            </Text>
            <View style={{ width: "100%", height: 200 }}>
              <Swiper
                loop={true}
                autoplay={true}
                renderPagination={renderPagination}
                autoplayTimeout={3}
                containerStyle={{
                  width: "100%",
                  height: "100%",
                  borderRadius: 5,
                  overflow: "hidden",
                  borderColor: "grey",
                  borderWidth: 0.5,
                }}
              >
                <View
                  key={"i"}
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "white",
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ fontSize: 16, textAlign: "center" }}>
                    배너광고
                  </Text>
                </View>
              </Swiper>
            </View>
          </View>

          {/*  요일별로 다를경우 해당부분 for문   */}
        </View>
      </View>
    </ScrollView>
  );
}

export default ShopInfo;

const styles = StyleSheet.create({
  marker: {
    borderWidth: 10,
    borderRadius: 50,
    borderColor: "rgb(255, 102, 0)",
  },
});
