/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

const onSuccess = async e => {
    console.log(e);
};

function QRcode({navigation}) {
  
  return (
    <View>
        <QRCodeScanner fadeIn={true} containerStyle={{backgroundColor:'rgba(0,0,0,0.5)'}} cameraStyle={{ alignSelf: 'center',
      justifyContent: 'center',
      width: '100%',height:Dimensions.get('window').height - 55}}
      vibrate={false}
          onRead={onSuccess.bind(this)} showMarker={true} markerStyle={styles.marker}
          flashMode={RNCamera.Constants.FlashMode.auto} reactivate={true} reactivateTimeout={3000} cameraTimeout={0} cameraProps={{ notAuthorizedView: (<View><Text>Not authorized</Text></View>) }}
        />
    </View>
  );
};



export default QRcode;

const styles = StyleSheet.create({

    marker: {
      borderWidth: 10,
      borderRadius: 50,
      borderColor: 'rgb(255, 102, 0)',
    }
  });
  
