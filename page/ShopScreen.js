/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect, useRef } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
//import { ImageHeaderScrollView, TriggeringView } from 'react-native-image-header-scroll-view';
import  ImageHeaderScrollView , { TriggeringView } from 'react-native-image-header-scroll-view';
import * as Animatable from 'react-native-animatable';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';

const list = [
  {idx:1,
  title:"통삼겹 김치찜",
  content:"1인 통삼겹 김치찜 + 조미김 + 공기밥1",
  price:15000,
  thumbnail:'https://mblogthumb-phinf.pstatic.net/MjAyMDEwMjlfMTI1/MDAxNjAzOTcyMDU2NTkw.PDiGOlo6Z3bHeEWJQOQr0G2zqhr8kODAXUAErYoDXOEg.JUwP0CUKV8n5HbJorrg3o14Ns6czGRO5GxEhRRfOzKgg.PNG.unmix/14.png?type=w800',
  type:1,
  recommend:true
},
{idx:2,
  title:"파송송고추장불고기",
  content:"불맛을 입힌 촉촉하고 부드러운 돼지고기에 고소한 참기름과 파 송송 넣은 불고기",
  price:21000,
  thumbnail:'https://recipe1.ezmember.co.kr/cache/recipe/2015/09/15/42b5867aaa46e4b7d2403de2fc1b5d70.jpg',
  type:1,
  recommend:true
},
{idx:3,
  title:"된장찌개",
  content:"ㅇㅇ 된장찌개에용..",
  price:8000,
  thumbnail:'https://recipe1.ezmember.co.kr/cache/recipe/2016/04/21/8477f28bfdd8ff7e50fa715892dd909d1.jpg',
  type:1,
  recommend:true
},
{idx:4,
  title:"일일 도시락",
  content:"매일매일 메뉴가 바뀌는 도시락 ㅇㅅㅇ",
  price:5000,
  thumbnail:'https://img.allurekorea.com/allure/2018/06/style_5b14c8d8bab9d.jpg',
  type:2,
  recommend:false
},
{idx:5,
  title:"소불고기세트",
  content:"말그대로 불고기세트 (공기밥 포함)",
  price:34000,
  thumbnail:'https://shop3.daumcdn.net/thumb/R300x300.q90/?fname=http%3A%2F%2Fshop3.daumcdn.net%2Fshophow%2Fp%2FA5086221676.jpg%3Fut%3D20191030153233',
  type:3,
  recommend:false
},
{idx:6,
  title:"된찌세트",
  content:"된찌 + 공기밥",
  price:9000,
  thumbnail:'https://recipe1.ezmember.co.kr/cache/recipe/2016/04/21/8477f28bfdd8ff7e50fa715892dd909d1.jpg',
  type:3,
  recommend:false
},
{idx:7,
  title:"감자튀김",
  content:"감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김감자튀김",
  price:3000,
  thumbnail:'https://img.seoul.co.kr/img/upload/2022/01/12/SSI_20220112170537_O2.jpg',
  type:4,
  recommend:true
},
];

const select = [
  {
    name:'추천메뉴',
    num:0},
    {
      name:'메인메뉴',
      num:1},
      {
        name:'1인분',
        num:2},
        {
          name:'세트메뉴',
          num:3},
          {
            name:'사이드메뉴',
            num:4},
];


function Shop({navigation}) {

  const [titleView, setTitleView] = useState(false);
  const [selectTitle, setSelectTitle] = useState({"name": "추천메뉴", "num": 0});

  const titleViewChange = () => {
    if(titleView == true){
      setTitleView(false);
      navigation.setOptions({
        title:'',
        //headerShown: false,
    headerLeft:()=>(
        <IconMaterial
        size={25}
        style={{paddingLeft:10}}
        name="keyboard-arrow-left"
        color={'white'}
        onPress={()=>navigation.goBack()}
    />
    ),
    headerRight:()=>(
        <View style={{
            flexDirection:'row',
            marginRight:10,
            width:70,
            justifyContent:'space-between'
        }}>
          
    <IconFeather
        size={25}
        name="home"
        color={'white'}
    />
    <View>
    
        <IconFeather
        size={25}
        name="shopping-bag"
        color={'white'}
    />
            </View>
        
             </View>
    ),
    headerTransparent: true,
        
     })
    }else{
      setTitleView(true);
      navigation.setOptions({
        title:'',
        //headerShown: false,
    headerLeft:()=>(
        <IconMaterial
        size={25}
        style={{paddingLeft:10}}
        name="keyboard-arrow-left"
        color={'black'}
        onPress={()=>navigation.goBack()}
    />
    ),
    headerRight:()=>(
        <View style={{
            flexDirection:'row',
            marginRight:10,
            width:70,
            justifyContent:'space-between'
        }}>
          
    <IconFeather
        size={25}
        name="home"
        color={'black'}
    />
    <View>
    
        <IconFeather
        size={25}
        name="shopping-bag"
        color={'black'}
    />
            </View>
        
             </View>
    ),
    headerTransparent: true,
        
     })
    }
  }

  const selectMenu = (select) => {

    console.log(select);
    navigation.push('SubPage',{
      screen:'MenuDetail',
      params:select, 
  }
  )
  }

  return (
    <View style={{ flex: 1 }}>
   <ImageHeaderScrollView
    minHeight={50}
    maxHeight={Dimensions.get('window').height/3}
       minOverlayOpacity={0}
      maxOverlayOpacity={1}
      overlayColor={'white'}

      renderFixedForeground={() => (
        titleView ? 
        <Animatable.View style={{height:50,
          justifyContent:'center',
          width:'50%',
          alignSelf:'center'}}>
          <Text ellipsizeMode='tail' numberOfLines={1} 
          style={{color:'black', textAlign:'center', fontSize:20,
          }}>놀부 김치찜 - 정부청사역점</Text>
        </Animatable.View> : <></>
      )}
    
      renderHeader={() => (
      <Image
        style={{
          width:Dimensions.get('window').width,
          height:Dimensions.get('window').height/3
        }}
        resizeMode='cover'
        source={{uri:'https://www.foodnews.news/data/photos/20220937/art_16633105549838_336582.jpg'}}/>)}
  >

    <TriggeringView
   // onHide={() => console.log("On Hide")}
    onDisplay={() =>  titleViewChange()}
    onBeginHidden={() => titleViewChange()}
   // onBeginDisplayed={() => console.log("OnBeginDisPlayed")}
    >
      <View style={{width:'90%', alignSelf:'center'}}>
        <View style={{width:'100%', 
        flexDirection:'column',
         justifyContent:'center',
          alignItems:'center', 
          paddingBottom:20,
          paddingTop:20}}>
          <View style={{flexDirection:'row', 
          justifyContent:'center', 
          alignItems:'center', 
          paddingBottom:5}}>
          <Text style={{fontSize:25, 
            fontWeight:'bold',
             color:'black'}}>놀부 김치찜 - 정부청사역점</Text>
          <IconFontAwesome
          style={{paddingLeft:10}}
        size={25}
        name="heart-o"
        color={'black'}
    />
    </View>
    <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
    <IconFontAwesome
        style={{paddingRight:10}}
        size={25}
        name="star"
        color={'yellow'}
    />
    <Text style={{fontSize:15, color:'black', fontWeight:'bold'}}>4.5 (213)</Text>
      </View>

        </View>
    <View style={{flexDirection:'column', paddingBottom:10}}>
    <Text style={{fontSize:15, color:'black', paddingBottom:5}}>조리시간 : 15-20분</Text>
    <TouchableOpacity
    style={{flexDirection:'row', alignItems:'center'}}
    onPress={() => {
      console.log("ASD")
    }}>
       <Text style={{fontSize:15, color:'black'}}>매장 정보</Text>
       <IconMaterial
                    size={20}
                    name="keyboard-arrow-right"
                    color={'black'}
                />
    </TouchableOpacity>
    </View>

      <View style={{
        height:50,
        borderColor:'grey',
        borderBottomWidth:0.5, 
        borderTopWidth:0.5,
        marginBottom:20,
      }}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
        horizontal={true}>
              {select.map((con, i) => {
                return(
                  <TouchableOpacity
                  onPress={() => setSelectTitle(con)}
                  key={i}
                  style={{backgroundColor:
                    selectTitle['num'] == i ?
                    "black" : 'white',
                  alignSelf:'center',
                  marginRight:5,
                   height:30,
                    alignItems:'center',
                    justifyContent:'center',
                    borderRadius:20,
                    }}>
                  <Text style={{fontSize:15, color:selectTitle['num'] == i ?
                    "white" : 'black', padding:5}}>{select[i]['name']}</Text>
                  </TouchableOpacity>
                )
              })}
        
        </ScrollView>
      </View>
      <Text style={{color:'grey', fontSize:16, paddingBottom:20, fontWeight:'bold'}}>{selectTitle['name']}</Text>
      {list.map((con, i) => {
        if(selectTitle['num'] == 0){
                  return(
                    con['recommend'] == true ?
                   <TouchableOpacity
                   onPress={()=>selectMenu(con)}
                   key={i}
                   style={{ width:'100%',
                    borderBottomColor:'grey',
                     borderBottomWidth:0.7, 
                     flexDirection:'row',
                     paddingBottom:20,
                     paddingTop:20,
                     }}>
                    <View style={{
                      flex:1,
                    flexDirection:'column',
                  backgroundColor:'white'}}>
               <Text
               ellipsizeMode='tail' numberOfLines={1}
               style={{color:'black', fontSize:18, paddingBottom:5, fontWeight:'bold'}}>{con['title']}</Text>
               <Text 
               ellipsizeMode='tail' numberOfLines={2} 
               style={{color:'grey', fontSize:16, paddingBottom:5, fontWeight:'400'}}>{con['content']}</Text>
                 <Text 
               style={{color:'black', fontSize:16, paddingBottom:5, fontWeight:'bold'}}>{
                (new String(con['price'])).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
               }원</Text>
                      </View>
                    <View style={{width:120, alignItems:'flex-end'}}>
                      <Image
        style={{width:100, height:100}}
        resizeMode='cover'
        source={{uri:con['thumbnail']}}/>
                  </View>
                    </TouchableOpacity> : <></>
                  )
        }else{
          return(
            con['type'] == selectTitle['num'] ?
            <TouchableOpacity
            onPress={()=>selectMenu(con)}
            key={i}
            style={{ width:'100%',
                    borderBottomColor:'grey',
                     borderBottomWidth:0.7, 
                     flexDirection:'row',
                     paddingBottom:20,
                     paddingTop:20,
                     }}>
                    <View style={{
                      flex:1,
                    flexDirection:'column',
                  backgroundColor:'white'}}>
               <Text
               ellipsizeMode='tail' numberOfLines={1}
               style={{color:'black', fontSize:18, paddingBottom:5, fontWeight:'bold'}}>{con['title']}</Text>
               <Text 
               ellipsizeMode='tail' numberOfLines={2} 
               style={{color:'grey', fontSize:16, paddingBottom:5, fontWeight:'400'}}>{con['content']}</Text>
                 <Text 
               style={{color:'black', fontSize:16, paddingBottom:5, fontWeight:'bold'}}>{
                (new String(con['price'])).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
               }원</Text>
                      </View>
                    <View style={{width:120, alignItems:'flex-end'}}>
                      <Image
        style={{width:100, height:100}}
        resizeMode='cover'
        source={{uri:con['thumbnail']}}/>
                  </View>
                    </TouchableOpacity> : <></>
          )
        }
                })}

      <View style={{width:'100%', height:60}}/>
      </View>
    </TriggeringView>

        </ImageHeaderScrollView>

        <View style={{position:'absolute',
         width:'90%',
          height:50, 
          backgroundColor:'rgba(255, 102, 0, 1)', 
          alignSelf:'center',
          borderRadius:5,
          alignItems:'center',
          justifyContent:'center',
          bottom:10}}>

 <Text style={{fontSize:18, color:'white', fontWeight:'400'}}>장바구니 가기 32,900원</Text>
            </View>
    </View>
    
  );
};



export default Shop;

const styles = StyleSheet.create({
  header: {
    height: 150,
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    backgroundColor: "transparent",
    color: "#fff",
    fontSize: 24
  },
  content: {
    padding: 20,
    height:1000
  },
  title: {
    fontSize: 36,
    fontWeight: "bold",
    marginBottom: 20
  },
  paragraph: {
    fontSize: 18,
    marginBottom: 20
  }
});
