/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
} from "react-native";
import Swiper from "react-native-swiper";
import IconEntypo from "react-native-vector-icons/Entypo";
import IconMaterial from "react-native-vector-icons/MaterialIcons";
import "../locale/index";
import { useTranslation } from "react-i18next";
import { changeLanguage } from "i18next";

const renderPagination = (index, total, context) => {
  return (
    <View
      style={{
        position: "absolute",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        right: 20,
        bottom: 10,
        borderRadius: 10,
      }}
    >
      <Text
        style={{
          color: "white",
          fontSize: 12,
          fontFamily: "KoPubWorldDotumBold",
          paddingHorizontal: 10,
        }}
      >
        1/1
      </Text>
    </View>
  );
};

function MainScreen({ navigation }) {
  const { t, i18n } = useTranslation();
  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ flex: 1, backgroundColor: "white", padding: "3%" }}>
        <Swiper
          loop={true}
          autoplay={true}
          renderPagination={renderPagination}
          autoplayTimeout={3}
          containerStyle={{
            width: "100%",
            height: "100%",
            borderRadius: 5,
            overflow: "hidden",
            borderColor: "grey",
            borderWidth: 0.5,
          }}
        >
          <View
            key={"i"}
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "white",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontSize: 16, textAlign: "center" }}>배너광고</Text>
          </View>
        </Swiper>
      </View>
      <View style={{ flex: 0.25, padding: "3%", paddingTop: 0 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            borderRadius: 5,
            borderColor: "grey",
            borderWidth: 0.5,
            justifyContent: "center",
          }}
        >
          <Text style={{ fontSize: 16, textAlign: "center" }}>주문내역</Text>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          paddingTop: 0,
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flex: 0.48,
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity
            onPress={() =>
              navigation.push("SubPage", {
                screen: "Shop",
              })
            }
            style={{
              flex: 0.45,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white",
              borderColor: "grey",
              borderWidth: 0.5,
              borderRadius: 5,
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Image
                style={{ width: 80, height: 80 }}
                source={require("../assets/images/default_logo.png")}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontSize: 16,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                {t("매장주문")}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              navigation.push("SubPage", {
                screen: "QRcode",
              })
            }
            style={{
              flex: 0.45,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white",
              borderColor: "grey",
              borderWidth: 0.5,
              borderRadius: 5,
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Image
                style={{ width: 80, height: 80 }}
                source={require("../assets/images/default_logo.png")}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontSize: 16,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                QR로 바로주문
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 0.48,
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity
            onPress={() => changeLanguage("ko")}
            style={{
              flex: 0.45,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white",
              borderColor: "grey",
              borderWidth: 0.5,
              borderRadius: 5,
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Image
                style={{ width: 80, height: 80 }}
                source={require("../assets/images/default_logo.png")}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontSize: 16,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                (임시)한국어
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => changeLanguage("en")}
            style={{
              flex: 0.45,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white",
              borderColor: "grey",
              borderWidth: 0.5,
              borderRadius: 5,
            }}
          >
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Image
                style={{ width: 80, height: 80 }}
                source={require("../assets/images/default_logo.png")}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontSize: 16,
                  color: "black",
                  fontWeight: "bold",
                }}
              >
                (임시)영어
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ flex: 0.25, padding: "3%" }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            borderRadius: 5,
            borderColor: "grey",
            borderWidth: 0.5,
            alignItems: "center",
            padding: "3%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <IconEntypo size={25} name="shop" color={"black"} />
            <Text
              style={{
                fontSize: 16,
                paddingLeft: "5%",
                fontSize: 18,
                color: "grey",
              }}
            >
              전체 매장 위치 보기
            </Text>
          </View>
          <IconMaterial size={30} name="keyboard-arrow-right" color={"black"} />
        </View>
      </View>
    </View>
  );
}

export default MainScreen;
