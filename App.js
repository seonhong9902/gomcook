/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  KeyboardAvoidingView,
  Text,
  useColorScheme,
  View,
  Platform
} from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import GomNavi from './navigator';
import reducer from './reducer';

let store = createStore(reducer);
export default function App() {
  return(
    <Provider store={store}>
      <View style={{height:getStatusBarHeight(true), width:'100%', backgroundColor:'white'}}></View>
      <KeyboardAvoidingView
          behavior={Platform.select({ios: 'padding', android: undefined})}
          style={styles.avoid}>
      <GomNavi />
      </KeyboardAvoidingView>
      </Provider>
  )
};

const styles = StyleSheet.create({
  avoid:{
    flex:1
  }

});