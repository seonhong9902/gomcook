import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Platform,
  Image,
  ImageBackground,
  Dimensions,
  Linking,
  SafeAreaView,
  StatusBar,
  Modal,
  BackHandler,
} from "react-native";
import Overlay from "react-native-modal-overlay";
import IconMaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
function MyTaste(props) {
  return (
    <Modal onRequestClose={props.onClose} style={styles.modalOverlay}>
      <IconMaterialCommunityIcons
        size={24}
        style={{ alignSelf: "flex-end", paddingRight: 10, paddingTop: 10 }}
        name="close"
        color={"black"}
        onPress={props.onClose}
      />

      <ScrollView style={{ paddingHorizontal: "5%", paddingVertical: 20 }}>
        <View style={{ width: "100%", height: 60, flexDirection: "row" }}>
          <View style={{ backgroundColor: "rgb(0, 204, 153)", flex: 1 }}></View>

          <View style={{ backgroundColor: "blue", flex: 1 }}></View>

          <View style={{ backgroundColor: "red", flex: 1 }}></View>
        </View>
      </ScrollView>
    </Modal>
  );
}

export default MyTaste;

const styles = StyleSheet.create({
  modalOverlay: {
    width: "100%",
    height: "100%",
    borderColor: "black",
    position: "absolute",
    zIndex: 9999,
    backgroundColor: "white",
  },
});
