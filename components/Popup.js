import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Platform,
  Image,
  ImageBackground,
  Dimensions,
  Linking,
  SafeAreaView,
  StatusBar,
} from "react-native";
import Overlay from "react-native-modal-overlay";

function Popup(props) {
  return (
    <Overlay
      childrenWrapperStyle={styles.modalOverlay}
      visible={props.visible}
      onClose={props.onClose}
    >
      <Text
        style={{
          fontSize: 15,
          color: "rgb(25, 25, 25)",
          paddingHorizontal: "5%",
          paddingVertical: "20%",
        }}
      >
        문의하실 내용을 입력해 주세요
      </Text>
    </Overlay>
  );
}

export default Popup;

const styles = StyleSheet.create({
  modalOverlay: {
    width: "70%",
    alignSelf: "center",
    borderColor: "black",
    borderWidth: 1,
  },
});
