/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import IconMaterial from "react-native-vector-icons/MaterialIcons";
import { requestMultiple, PERMISSIONS } from "react-native-permissions";
import AsyncStorage from "@react-native-async-storage/async-storage";

function Term({ navigation }) {
  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log("Authorization status:", authStatus);
    }
  }

  const permissionCheck = () => {
    console.log("??");
    if (Platform.OS == "android") {
      requestMultiple([
        PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
        PERMISSIONS.ANDROID.POST_NOTIFICATIONS,
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
        PERMISSIONS.ANDROID.CAMERA,
        PERMISSIONS.ANDROID.READ_MEDIA_IMAGES,
      ]).then(async (statuses) => {
        if (
          statuses[PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION] === "granted" &&
          statuses[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === "granted" &&
          statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] === "granted" &&
          statuses[PERMISSIONS.ANDROID.POST_NOTIFICATIONS] === "granted" &&
          statuses[PERMISSIONS.ANDROID.CAMERA] === "granted" &&
          statuses[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE] === "granted" &&
          statuses[PERMISSIONS.ANDROID.READ_MEDIA_IMAGES] === "granted"
        ) {
          AsyncStorage.setItem("myPermissions", "granted");
          navigation.replace("Tab");
        } else {
          AsyncStorage.setItem("myPermissions", "granted");
          navigation.replace("Tab");
        }
      });
    } else {
      requestMultiple([
        PERMISSIONS.IOS.LOCATION_ALWAYS,
        PERMISSIONS.IOS.PHOTO_LIBRARY,
        PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      ]).then(async (statuses) => {
        if (
          statuses[PERMISSIONS.IOS.LOCATION_ALWAYS] === "granted" &&
          statuses[PERMISSIONS.IOS.PHOTO_LIBRARY] === "granted" &&
          statuses[PERMISSIONS.IOS.LOCATION_WHEN_IN_USE] === "granted"
        ) {
          requestUserPermission();
          AsyncStorage.setItem("myPermissions", "granted");
          navigation.replace("Tab");
        } else {
          requestUserPermission();
          AsyncStorage.setItem("myPermissions", "granted");
          navigation.replace("Tab");
        }
      });
    }
  };

  return (
    <View
      style={{
        width: "100%",
        height: "100%",
        backgroundColor: "white",
        alignItems: "center",
        padding: 15,
      }}
    >
      <View
        style={{
          borderColor: "rgba(0,0,0,0.1)",
          width: "90%",
          borderRadius: 10,
          borderWidth: 1,
          alignItems: "center",
          marginTop: "15%",
        }}
      >
        <Image
          style={{
            width: Dimensions.get("window").width / 6,
            height: Dimensions.get("window").width / 6,
            marginTop: 20,
          }}
          source={require("./assets/images/default_logo.png")}
          resizeMode={"contain"}
        />
        <Text
          style={{
            color: "black",
            fontSize: 20,
            textAlign: "center",
            marginTop: 20,
            fontWeight: "400",
            marginBottom: 40,
          }}
        >
          곰쿡을 즐기기위해{"\n"}다음 권한의 허용이 필요합니다.
        </Text>

        <View
          style={{
            width: "90%",
            padding: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 40,
              height: 40,
              backgroundColor: "white",
              borderRadius: 999,
              alignItems: "center",
              justifyContent: "center",
              borderColor: "rgba(0,0,0,0.1)",
              borderWidth: 1.5,
            }}
          >
            <IconMaterial size={24} name="smartphone" color={"black"} />
          </View>

          <View style={{ flexDirection: "column", paddingLeft: 10 }}>
            <Text style={{ color: "black", fontSize: 14, fontWeight: "400" }}>
              기기 및 앱기록 (필수)
            </Text>
            <Text
              style={{
                color: "rgb(118, 118, 118)",
                fontSize: 14,
                fontWeight: "400",
              }}
            >
              앱 오류 확인 및 사용성 개선
            </Text>
          </View>
        </View>

        <View
          style={{
            width: "90%",
            padding: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 40,
              height: 40,
              backgroundColor: "white",
              borderRadius: 999,
              alignItems: "center",
              justifyContent: "center",
              borderColor: "rgba(0,0,0,0.1)",
              borderWidth: 1.5,
            }}
          >
            <IconMaterial size={24} name="place" color={"black"} />
          </View>

          <View style={{ flexDirection: "column", paddingLeft: 10 }}>
            <Text style={{ color: "black", fontSize: 14, fontWeight: "400" }}>
              위치 (필수)
            </Text>
            <Text
              style={{
                color: "rgb(118, 118, 118)",
                fontSize: 14,
                fontWeight: "400",
              }}
            >
              사진/카메라 (선택)
            </Text>
          </View>
        </View>

        <View
          style={{
            width: "90%",
            padding: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 40,
              height: 40,
              backgroundColor: "white",
              borderRadius: 999,
              alignItems: "center",
              justifyContent: "center",
              borderColor: "rgba(0,0,0,0.1)",
              borderWidth: 1.5,
            }}
          >
            <IconMaterial size={24} name="camera-alt" color={"black"} />
          </View>

          <View style={{ flexDirection: "column", paddingLeft: 10 }}>
            <Text style={{ color: "black", fontSize: 14, fontWeight: "400" }}>
              사진/카메라 (선택)
            </Text>
            <Text
              style={{
                color: "rgb(118, 118, 118)",
                fontSize: 14,
                fontWeight: "400",
              }}
            >
              리뷰 작성시 사진촬영 및 첨부기능
            </Text>
          </View>
        </View>

        <View
          style={{
            width: "90%",
            padding: 10,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: 40,
              height: 40,
              backgroundColor: "white",
              borderRadius: 999,
              alignItems: "center",
              justifyContent: "center",
              borderColor: "rgba(0,0,0,0.1)",
              borderWidth: 1.5,
            }}
          >
            <IconMaterial size={24} name="folder" color={"black"} />
          </View>

          <View style={{ flexDirection: "column", paddingLeft: 10 }}>
            <Text style={{ color: "black", fontSize: 14, fontWeight: "400" }}>
              저장공간 (선택)
            </Text>
            <Text
              style={{
                color: "rgb(118, 118, 118)",
                fontSize: 14,
                fontWeight: "400",
              }}
            >
              이벤트, 쿠폰관련 이미지 및 포인트정보
            </Text>
          </View>
        </View>

        <View
          style={{
            width: "90%",
            borderTopColor: "rgb(237, 237, 237)",
            borderTopWidth: 1,
            marginTop: 10,
            paddingVertical: 10,
          }}
        >
          <Text
            style={{
              color: "rgb(118, 118, 118)",
              fontSize: 12,
              fontWeight: "400",
            }}
          >
            선택적 접근권한은 해당 기능을 사용시 동의를 받고 있습니다. 동의하지
            않으셔도 서비스 이용이 가능합니다.
          </Text>
          <View
            style={{
              width: "100%",
              paddingTop: 20,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            {/* <View style={{
                    width:'48%',
                    height:44,
                    backgroundColor:'white',
                    borderRadius:5,
                    borderColor:'rgb(237, 237, 237)',
                    borderWidth:1,
                    justifyContent:'center'
                  }}>
                    <Text style={{color:'rgb(118, 118, 118)', fontSize:16, fontWeight:'400', textAlign:'center'}}>
                      취소
                    </Text>
                  </View> */}

            <TouchableOpacity
              onPress={() => permissionCheck()}
              style={{
                width: "100%",
                height: 44,
                borderRadius: 5,
                backgroundColor: "rgb(255, 102, 0)",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: 16,
                  fontWeight: "400",
                  textAlign: "center",
                }}
              >
                동의
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

export default Term;
