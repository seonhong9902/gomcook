import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Platform,
  Dimensions,
} from "react-native";
import React, { useState } from "react";
import IconMaterial from "react-native-vector-icons/MaterialIcons";
import IconFontAwesome from "react-native-vector-icons/FontAwesome";
import IconFeather from "react-native-vector-icons/Feather";
import Splash from "./SplashScreen";
import Main from "./page/mainScreen";
import QRcode from "./page/qrScreen";
import Shop from "./page/ShopScreen";
import Term from "./TermScreen";
import MenuDetail from "./page/menuDetail";
import MyMap from "./page/map/myMap";
import SetMyAddress from "./page/map/setMyAddress";
import SearchGeo from "./page/map/searchGeo";
import AlarmCenter from "./page/alarmCenter";
import MyPageMain from "./page/myPage/myPageMain";
import EditMyPassword from "./page/myPage/editMyPassword";
import EditUserInfo from "./page/myPage/editUserInfo";
import MyPageInfo from "./page/myPage/myPageInfo";
import { useTranslation } from "react-i18next";
import LeaveReviews from "./page/myPage/leaveReviews";

const Stack = createStackNavigator();
const SubStack = createStackNavigator();
const Tab = createStackNavigator();

export default function GomNavi({ navigation }) {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Term"
          component={Term}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Tab"
          component={Tabs}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="SubPage"
          component={SubStackScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function Tabs({ navigation }) {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Main"
        component={Main}
        options={{
          title: "",
          //headerShown: false,
          headerLeft: () => (
            <View
              style={{
                flexDirection: "row",
              }}
            >
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  fontSize: 20,
                  paddingLeft: 10,
                  fontWeight: "bold",
                }}
              >
                영등포구 선유로 9길 10 SK V1 Center
              </Text>

              <IconMaterial
                onPress={() =>
                  navigation.push("SubPage", {
                    screen: "SetMyAddress",
                  })
                }
                size={25}
                name="keyboard-arrow-down"
                color={"black"}
              />
            </View>
          ),
          headerRight: () => (
            <View
              style={{
                flexDirection: "row",
                marginRight: 10,
                width: 70,
                justifyContent: "space-between",
              }}
            >
              <View>
                <View
                  style={{
                    position: "absolute",
                    backgroundColor: "red",
                    width: 15,
                    height: 15,
                    top: 0,
                    right: 0,
                    borderRadius: 999,
                    zIndex: 2,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontSize: 12,
                      fontWeight: "bold",
                    }}
                  >
                    1
                  </Text>
                </View>
                <IconFontAwesome
                  size={25}
                  name="bell-o"
                  color={"black"}
                  onPress={() =>
                    navigation.push("SubPage", {
                      screen: "AlarmCenter",
                    })
                  }
                />
              </View>
              <IconFontAwesome
                size={25}
                name="user-o"
                color={"black"}
                onPress={() =>
                  navigation.push("SubPage", {
                    screen: "MyPage",
                  })
                }
              />
            </View>
          ),
          headerStyle: {
            backgroundColor: "white",
            height: 50,
          },
          headerTintColor: "white",
          headerTitleAlign: "center",
        }}
      ></Tab.Screen>
    </Tab.Navigator>
  );
}

function SubStackScreen({ navigation }) {
  const { t, i18n } = useTranslation();
  return (
    <SubStack.Navigator>
      <SubStack.Screen
        name="QRcode"
        component={QRcode}
        options={{
          title: `${t("qrCode")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={25}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="MenuDetail"
        component={MenuDetail}
        options={{
          title: "",
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={25}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"white"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <View
              style={{
                flexDirection: "row",
                marginRight: 10,
                width: 70,
                justifyContent: "space-between",
              }}
            >
              <IconFeather size={25} name="home" color={"white"} />
              <View>
                <IconFeather size={25} name="shopping-bag" color={"white"} />
              </View>
            </View>
          ),
          headerTransparent: true,
        }}
      />

      <SubStack.Screen
        name="SetMyAddress"
        component={SetMyAddress}
        options={{
          title: `${t("setLoc")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() =>
                navigation.push("Tab", {
                  screen: "Main",
                })
              }
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="MyMap"
        component={MyMap}
        options={{
          title: `${t("checkMyLoc")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="SearchGeo"
        component={SearchGeo}
        options={{
          title: `${t("searchLoc")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              // onPress={() => navigation.goBack()}
              onPress={() =>
                navigation.push("SubPage", {
                  screen: "SetMyAddress",
                })
              }
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="AlarmCenter"
        component={AlarmCenter}
        options={{
          title: `${t("notifications")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="MyPage"
        component={MyPageMain}
        options={{
          title: `${t("myPage")}`,
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() =>
                navigation.push("Tab", {
                  screen: "Main",
                })
              }
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="MyPageInfo"
        component={MyPageInfo}
        options={{
          title: `${t("myInfo")}`,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="EditMyPassword"
        component={EditMyPassword}
        options={{
          title: `${t("resetPW")}`,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="EditUserInfo"
        component={EditUserInfo}
        options={{
          title: `${t("editInfo")}`,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="LeaveReviews"
        component={LeaveReviews}
        options={{
          title: `${t("leaveReview")}`,
          headerLeft: () => (
            <IconMaterial
              size={30}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"black"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitleAlign: "center",
        }}
      />

      <SubStack.Screen
        name="Shop"
        component={Shop}
        options={{
          title: "",
          //headerShown: false,
          headerLeft: () => (
            <IconMaterial
              size={25}
              style={{ paddingLeft: 10 }}
              name="keyboard-arrow-left"
              color={"white"}
              onPress={() => navigation.goBack()}
            />
          ),
          headerRight: () => (
            <View
              style={{
                flexDirection: "row",
                marginRight: 10,
                width: 70,
                justifyContent: "space-between",
              }}
            >
              <IconFeather size={25} name="home" color={"white"} />
              <View>
                <IconFeather size={25} name="shopping-bag" color={"white"} />
              </View>
            </View>
          ),
          headerTransparent: true,
        }}
      />
    </SubStack.Navigator>
  );
}
