export default {
  // navigator
  매장주문: "매장주문",
  qrCode: "QR 코드 스캔",
  setLoc: "주소 설정",
  checkMyLoc: "지도에서 위치 확인",
  searchLoc: "주소 검색",
  notifications: "알림 센터",
  myPage: "마이페이지",
  myInfo: "내 정보",
  resetPW: "비밀번호 재설정",
  editInfo: "내 정보 변경",

  // 알림 센터
  pastNoti: "이전 알림 보기",
  InputDetailLoc: "상세 주소를 입력해주세요",
  setLoc: "이 위치로 주소 설정",
  searchPlaces: "지번, 도로명, 건물명 검색",
  setCurrLoc: "현재 위치로 설정",
  searchHistory: "최근 주소",

  // 마이페이지
  goLogin: "로그인 / 회원가입 하기",
  id: "아이디",
  password: "비밀번호",
  userName: "이름",
  userBirthDate: "생년월일",
  userGender: "성별",
  userNumber: "휴대전화",
  editMyInfo: "내 정보 변경",
  resetMobile: "등록된 휴대폰 번호로 재설정",
  typeInName: "이름",
  typeInMobile: "휴대폰 번호 입력 ('-' 없이)",
  getAuthNum: "인증 번호 받기",
  typeInAuthNum: "인증 번호를 입력하세요",
  checkAuthNum: "인증 번호 확인",

  // 마이페이지 - 내 정보 변경
  nickname: "닉네임",
  chooseChar: "곰쿡 캐릭터 선택",
  leaveReview: "리뷰 작성",
};
