import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from "./en.js";
import ko from "./ko.js";

const resources = {
  en: {
    translation: en,
  },
  ko: {
    translation: ko,
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: "ko",
  fallbackLng: "ko",
  compatibilityJSON: "v3",
  interpolation: {
    escapeValue: false,
  },
});
