export default {
  매장주문: "Store Order",
  qrCode: "Scan QR code",
  setLoc: "Setting Location",
  checkMyLoc: "Check My Location",
  searchAddress: "Search location",
  notifications: "Notifications",
  myPage: "My Page",
  myInfo: "Personal Information",
  resetPW: "Reset Password",
  editInfo: "Edit Personal Information",

  // 알림 센터
  pastNoti: "Notifications History",
  InputDetailLoc: "Type In Location Details",
  setLoc: "Set Location",
  searchPlaces: "Search Place or Address ...",
  setCurrLoc: "Set Current Location",
  searchHistory: "Search History",

  // 마이페이지
  goLogin: "Sign In or Sign Up",
  id: "ID",
  password: "Password",
  userName: "Name",
  userBirthDate: "Birth Date",
  userSex: "Sex",
  userNumber: "Mobile Number",
  editMyInfo: "Edit Personal Information",
  resetMobile: "Reset Mobile Number",
  typeInName: "First name / Last name",
  typeInMobile: "Type in mobile number without '-'",
  getAuthNum: "get a verification code",
  typeInAuthNum: "Type in the verification code",
  checkAuthNum: "verify the code",

  // 마이페이지 - 내 정보 변경
  nickname: "Nickname",
  chooseChar: "Choose Your Profile Character",
  leaveReview: "Leave a review",
};
