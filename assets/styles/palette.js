export const theme = {
  mainColor: "#FF6600", // 주황색 메인색
  subColor: "#00CC99", // 초록색 서브색
  blurColor: "#FF9B04", // 버튼 블러색
  backColor: "#F1F1F5", // 엹은 회색
  textColor: "#191919",
};
