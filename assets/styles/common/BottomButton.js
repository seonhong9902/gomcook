import React from "react";
import { View, StyleSheet, Button, Text, TouchableOpacity } from "react-native";
import { theme } from "../palette";

const BottomButton = ({ title_text }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Text style={styles.buttonText}>{`${title_text}`}</Text>
      </TouchableOpacity>
      {/* <Button color={`${theme.mainColor}`} title={`${title_text}`} /> */}
    </View>
  );
};

export default BottomButton;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    width: "100%",
    height: 55,
    backgroundColor: `${theme.mainColor}`,
    position: "absolute",
    bottom: 0,
    alignItems: "center",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
  },
});
