import { View } from "react-native";

const BlurBottomButton = () => {
  return (
    <View
      style={{
        position: "absolute",
        width: "90%",
        height: 50,
        backgroundColor: "rgba(255, 102, 0, 1)",
        alignSelf: "center",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        bottom: 10,
      }}
    ></View>
  );
};

export default BlurBottomButton;
