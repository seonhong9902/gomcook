/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  Dimensions,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

function Splash({ navigation }) {
  setTimeout(() => {
    AsyncStorage.getItem("myPermissions", (err, result) => {
      if (result == "granted") {
        navigation.replace("Tab");
      } else {
        navigation.replace("Term");
      }
    });
  }, 3000);

  return (
    <View>
      <View
        style={{
          width: "100%",
          height: "100%",
          backgroundColor: "rgb(255, 102, 0)",
          alignItems: "center",
          padding: 15,
        }}
      >
        <Image
          style={{
            width: Dimensions.get("window").width / 3,
            height: Dimensions.get("window").width / 3,
            marginTop: Dimensions.get("window").height / 12,
          }}
          source={require("./assets/images/default_logo.png")}
          resizeMode={"contain"}
        />
        <Text
          style={{
            fontSize: 40,
            color: "white",
            marginTop: Dimensions.get("window").width / 12,
            fontWeight: "bold",
          }}
        >
          GOMCOOK
        </Text>

        <Text
          style={{
            fontSize: 14,
            color: "white",
            textAlign: "center",
            position: "absolute",
            bottom: Dimensions.get("window").height / 15,
          }}
        >
          데이터통화료는 가입하신 요금제에 따라 차감 또는 별도 부과되며 해외
          로밍시, 국내와는 다른 별도 로밍 요금이 청구됩니다.
        </Text>
      </View>
    </View>
  );
}

export default Splash;
